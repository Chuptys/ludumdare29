﻿using UnityEngine;
using System.Collections;

public class TowerElement : MonoBehaviour {
	
	public float startHP;
	private float HP;
	public int price;
	void Start() {
		HP = startHP;
	}

	public void dealDamage(float damage){
		HP = HP - damage;
	}	
	
	public float getHP(){
		return HP;
	}
}
