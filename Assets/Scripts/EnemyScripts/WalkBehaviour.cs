﻿using UnityEngine;
using System.Collections;

public class WalkBehaviour {

	private WalkPath Path;

	private Vector3 nextPos;
	private float speed;

	private bool targetReached = false;

	public WalkBehaviour(WalkPath path, float walkSpeed)
	{
		Path = path;
		nextPos = Path.GetNextNode ();
		speed = walkSpeed;
	}

	public bool HasReachedFinalDestination()
	{
		return targetReached;
	}

	public Vector3 GetNextPosition(Vector3 currentPos)
	{
		checkPathNodes(currentPos);
		return getPosition (currentPos);
	}

	private void checkPathNodes(Vector3 currentPos)
	{
		var dist = Vector3.Distance (currentPos, nextPos);
		//Debug.Log ("Distance: "+dist);

		if (dist < 0.1f) {
			if (!Path.HasNext ()) {
				targetReached = true;
				return;
			}

			nextPos = Path.GetNextNode();
		}
	}

	private Vector3 getPosition(Vector3 currentPos)
	{
		Vector3 direction = nextPos - currentPos;
		direction.Normalize ();
		float distance = speed * Time.deltaTime;
		return currentPos + direction * distance;
	}

}
