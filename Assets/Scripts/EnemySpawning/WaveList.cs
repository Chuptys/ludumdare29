﻿using UnityEngine;
using System.Collections.Generic;

public abstract class WaveList {
	protected List<WaveData> waves = new List<WaveData>();
	private int index = -1;

	public bool HasNextWave()
	{
		return index < waves.Count;
	}
	
	public WaveData GetNextWaveData()
	{
		index ++;
		return waves [index];
	}
}
