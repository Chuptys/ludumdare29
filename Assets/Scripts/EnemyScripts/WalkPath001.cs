using UnityEngine;
using System.Collections.Generic;

public class WalkPath001 : WalkPath {


	protected override void addNodes()
	{
		Nodes.Add (new Vector3 (0, 0, 0));
		Nodes.Add (new Vector3 (10, 0, 0));
		Nodes.Add (new Vector3 (0, 0, 0));
	}

	public override WalkPath GetNew()
	{
		return new WalkPath001();
	}
}
