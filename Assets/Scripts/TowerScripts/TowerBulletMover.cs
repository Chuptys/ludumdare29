﻿using UnityEngine;
using System.Collections;

public class TowerBulletMover : BulletController {

	public float speed;

	void Start ()
	{
		rigidbody.velocity = transform.up * speed;
	}

}
