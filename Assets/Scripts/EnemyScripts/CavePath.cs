﻿using UnityEngine;
using System.Collections.Generic;

public class CavePath : WalkPath {

	private class StartEndTuple{
		public Vector3 Start = new Vector3();
		public List<Vector3> Ends = new List<Vector3>();
	}

	private List<StartEndTuple> paths;

	private float pathHeight = 0f;
	private float nbEndPointsPerSpawnPoint = 3f;
	private float angleDiffAtTower = Mathf.PI/8f;

	public CavePath(float towerRadius, float spawnRadius, int nbSpawnPoints)
	{
		paths = new List<StartEndTuple> ();

		var angle = 360f / (float)nbSpawnPoints/180f*Mathf.PI;
		for(int i=0; i<=nbSpawnPoints; i++)
		{
			var cTuple = new StartEndTuple();

			var dir = new Vector3(Mathf.Cos(i*angle), pathHeight/spawnRadius, Mathf.Sin(i*angle));
			cTuple.Start = dir*spawnRadius;

			for(float j=-nbEndPointsPerSpawnPoint*0.5f; j<=nbEndPointsPerSpawnPoint*0.5f; j++)
			{
				var tempAngle = i*angle+j*angleDiffAtTower;
				var dir02 = new Vector3(Mathf.Cos(tempAngle), pathHeight/towerRadius, Mathf.Sin(tempAngle));
				cTuple.Ends.Add(dir02*towerRadius);
			}

			paths.Add(cTuple);
		}

	}


	protected override void addNodes()
	{
		//do not implement
	}
	
	public override WalkPath GetNew()
	{
		//Debug.Log ("StartPointCount: "+starts.Count);
		//Debug.Log ("EndPointCount: "+ends.Count);

		var rand = Random.Range (0, paths.Count - 1);
		var selectedTuple = paths [rand];
		var rand01 = Random.Range (0, selectedTuple.Ends.Count - 1);

		return new TwoPointPath(selectedTuple.Start, selectedTuple.Ends[rand01]);
	}
}
