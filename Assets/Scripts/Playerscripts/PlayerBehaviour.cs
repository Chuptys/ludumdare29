﻿using UnityEngine;
using System.Collections;

public class PlayerBehaviour : MonoBehaviour {

	public GameObject PlayerBulletPrefab;

	public float startHealth;
	private float health;

	public float CooldownTime = 2;
	private float cooldown;

	void Start(){
		health = startHealth;
	}

	void Update () {
		cooldown -= Time.deltaTime;

		if (Input.GetMouseButton (0) && cooldown <= 0) {
			GameObject newElement = (Instantiate(PlayerBulletPrefab, new Vector3(), Quaternion.identity) as GameObject);
			PlayerBulletMover mover = newElement.GetComponent<PlayerBulletMover>();
			mover.Cam = GameObject.Find ("Main Camera");
			cooldown = CooldownTime;
		}
	}

	public void ReceiveDamage(float damage)
	{
		health -= damage;
		Debug.Log ("Player has "+health+" health left");

		if (health <= 0) {
			Debug.Log("Game Over");
		}
	}
}
