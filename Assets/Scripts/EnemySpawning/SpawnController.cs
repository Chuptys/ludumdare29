﻿using UnityEngine;
using System.Collections.Generic;

public class SpawnController : MonoBehaviour {

	private SpawnManager manager;
	public GameObject EnemyPrefab_Standard;	

	void Start(){
		manager = new SpawnManager ();

		var endPoints = new List<Vector3> ();
		endPoints.Add (new Vector3 (-50, 26, 20));
		endPoints.Add (new Vector3 (-55, 26, 20));
		endPoints.Add (new Vector3 (-60, 26, 20));
		endPoints.Add (new Vector3 (-45, 26, 20));
		endPoints.Add (new Vector3 (-40, 26, 20));

		manager.Path = new Easy2PointPath (new Vector3 (-50, 26, 50), endPoints);
		manager.WaveDataList = new WaveList01 ();

		manager.EnemyPrefab_Standard = this.EnemyPrefab_Standard;
	}

	void Update () {
		manager.Update();
	}

}
