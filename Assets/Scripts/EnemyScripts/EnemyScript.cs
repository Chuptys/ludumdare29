﻿using UnityEngine;
using System.Collections;

public class EnemyScript : MonoBehaviour {

	public float EnemyHeight;

	private GameObject player;
	private WalkBehaviour walkBehav;
	private AttackTowerBehaviour attackTowerBehav;
	private AttackPlayerBehaviour attackPlayerBehav;
	private EnemyState state;

	//private float walkSpeed = 2f;
	private float playerChaseSpeed = 3f;
	private float playerChaseRange = 5f;
	private float playerAttackRange = 2f;

    //animation vars
    private const float hoverFrequency = 0.5f;
    private const float hoverDistance = 0.2f;
    private Vector3 Spike1_off = new Vector3(-0.5926452f,-0.4561443f,-0.09040016f);
    private Vector3 Spike2_off = new Vector3(-0.2836689f,0.1492752f,-0.07720695f);
    private Vector3 SpikeA_off = new Vector3(0f,0f,0f);

	private float HP;
	public float startHP;
	private int healthBarWidth = 60;
	private int healthBarHeight = 40;

	private bool initialized = false;

	private enum EnemyState
	{
		WALK, ATTACKTOWER, ATTACKPLAYER, DEAD
	}

	// Use this for initialization
	void Start () {
		attackTowerBehav = new AttackTowerBehaviour (5f, 10f);
		attackPlayerBehav = new AttackPlayerBehaviour(5f, 10f, playerChaseSpeed);

		player = GameObject.Find ("First Person Controller");

		state = EnemyState.WALK;

		HP = startHP;
		Debug.Log (HP);
	}

	public void Setup(WalkPath walkPath, float walkSpeed, float enemyHeight)
	{
		walkBehav = new WalkBehaviour (walkPath, walkSpeed);
		EnemyHeight = enemyHeight;
		initialized = true;
	}

	void OnTriggerEnter(Collider other){
		Debug.Log (other.tag);
		if (other.tag == "Bullet") {
			HP -= other.GetComponent<BulletController>().damage;
			if(HP <= 0){
				Destroy (this.gameObject);
			}
			Destroy (other.gameObject);
		}
	}


	// Update is called once per frame
	void Update () {
		if (!initialized)
			return;

		switch (state) {
		case EnemyState.WALK:
			updateWalk();
			break;
		case EnemyState.ATTACKPLAYER:
			updateAttackPlayer();
			break;
		case EnemyState.ATTACKTOWER:
			updateAttackTower();
			break;
		case EnemyState.DEAD:
			die();
			break;
		}

        //hover animation
        Vector3 offset = new Vector3(0f,hoverDistance * (float)System.Math.Cos(Time.time*2*System.Math.PI*hoverFrequency),0f);
		if (this.transform.Find("Body") != null)
        {
            this.transform.Find("Body").transform.localPosition = offset;//body
            this.transform.Find("Spike_Body1").transform.localPosition = Spike1_off + offset;//spike 1
            this.transform.Find("Spike_Body2").transform.localPosition = Spike2_off + offset;//spike 2
            this.transform.Find("Spike_Attack").transform.localPosition = SpikeA_off + offset; //attack spike
        }
	}

	private void updateAttackPlayer()
	{
		attackPlayerBehav.UpdateCooldown();

		if (Vector3.Distance (transform.position, player.transform.position) < playerAttackRange) {
			//als speler heel dichtbij => attack
			attackPlayerBehav.Attack();
		} 
		else if (Vector3.Distance (transform.position, player.transform.position) < playerChaseRange) {
			//als speler dichtbij => chase
			MoveTo(attackPlayerBehav.getNewPositionForChase(transform.position, player.transform.position));
		}
		else {
			// als speler te ver => go back to walking
			state = EnemyState.WALK;
		}

	}

	private void updateAttackTower()
	{
		attackTowerBehav.Update ();
	}

	private void updateWalk()
	{
		if (Vector3.Distance (transform.position, player.transform.position) < playerChaseRange) {
			state = EnemyState.ATTACKPLAYER;
			return;
		}

		if (!walkBehav.HasReachedFinalDestination ()) {			
			MoveTo(walkBehav.GetNextPosition (transform.position));			
				} else {
						state = EnemyState.ATTACKTOWER;
				}	
	}

	private void die()
	{
		//todo
	}

	private float previousHP = 0;
	private GUIStyle currentHealthBarStyle;
	private Color red = new Color(1.0f, 0, 0);
	private Color green = new Color(0, 1.0f, 0);

	public void drawGui(Camera camera){
		if (HP != previousHP) {
			updateHPStyle();
			previousHP = HP;
			Debug.Log ("updateStyle called");
		}
		
		Vector2 targetPos;
		targetPos = camera.WorldToScreenPoint (transform.position);
		Vector3 viewPort = camera.WorldToViewportPoint (transform.position);
		
		if(viewPort.z > 0) GUI.Box(new Rect(targetPos.x - 30, Screen.height - (targetPos.y + 50), 60, 20), HP + "/" + startHP, currentHealthBarStyle );
	}

	/*void OnGUI()
		
	{
		if (HP != previousHP) {
			updateHPStyle();
			previousHP = HP;
			Debug.Log ("updateStyle called");
		}

		Vector2 targetPos;
		targetPos = playerCamera.WorldToScreenPoint (transform.position);
		Vector3 viewPort = playerCamera.WorldToViewportPoint (transform.position);
		
		if(viewPort.z > 0) GUI.Box(new Rect(targetPos.x - 30, Screen.height - (targetPos.y + 50), 60, 20), HP + "/" + startHP, currentHealthBarStyle );
	}*/
	
	private void updateHPStyle()
		
	{
		currentHealthBarStyle = new GUIStyle (GUI.skin.box);
		currentHealthBarStyle.normal.background = MakeHPTex();
		
	}
	
	
	
	private Texture2D MakeHPTex()
		
	{
		
		Color[] pix = new Color[healthBarWidth * healthBarHeight];
		float ratio = HP / startHP;
		Debug.Log ("ratio: " + ratio);

		float pixelratio = 0;

		for( int i = 0; i < pix.Length; ++i )
			
		{
			pixelratio = ((float) (i % healthBarWidth)) / healthBarWidth;
			if(pixelratio <= ratio){
				pix [ i ] = green;
			}
			else{
				pix[i] = red;
			}
			
		}
		
		Texture2D result = new Texture2D( healthBarWidth, healthBarHeight);
		
		result.SetPixels( pix );
		
		result.Apply();
		
		return result;		
	}

	public void MoveTo(Vector3 pos)
	{
		var newPos = new Vector3 (pos.x, EnemyHeight, pos.z);
		var currentPos = transform.position;
		var dir = newPos - currentPos;
		dir.Normalize ();

		if (dir != new Vector3 (0, 0, 0)) {
			var rot = Quaternion.LookRotation (dir);
			var newRot = Quaternion.Lerp (transform.rotation, rot, 0.05f);
			transform.rotation = newRot;
		}

		transform.position = newPos;

	}


}
