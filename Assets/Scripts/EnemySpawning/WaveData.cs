﻿using UnityEngine;
using System.Collections.Generic;

public class WaveData {
	public List<float> SpawnTimes = new List<float> ();
	public List<SpawnManager.EnemyTypes> EnemyList = new List<SpawnManager.EnemyTypes>();

	public void AddEnemyAtTime(SpawnManager.EnemyTypes type, float time){
		SpawnTimes.Add (time);
		EnemyList.Add (type);
	}
}


