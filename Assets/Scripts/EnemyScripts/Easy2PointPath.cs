﻿using UnityEngine;
using System.Collections.Generic;

public class Easy2PointPath : WalkPath {

	private Vector3 startNode;
	private List<Vector3> endNodes;

	public Easy2PointPath(Vector3 start, List<Vector3> ends)
	{
		startNode = start;
		endNodes = ends;
	}

	protected override void addNodes()
	{
		//do not implement
	}

	public override WalkPath GetNew()
	{
		var rand = Random.Range (0, endNodes.Count - 1);
		return new TwoPointPath(startNode, endNodes[rand]);
	}
}
