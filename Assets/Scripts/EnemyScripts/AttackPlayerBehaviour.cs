﻿using UnityEngine;
using System.Collections;

public class AttackPlayerBehaviour {

	private float damage;
	private float cooldown = 0f;
	private float maxCoolDown;
	private float speed;

	private PlayerBehaviour player = GameObject.FindObjectOfType(typeof(PlayerBehaviour)) as PlayerBehaviour;

	public AttackPlayerBehaviour(float cooldownValue, float damageValue,  float chaseSpeed)
	{
		damage = damageValue;
		maxCoolDown = cooldownValue;
		speed = chaseSpeed;
	}

	public Vector3 getNewPositionForChase(Vector3 enemyPos, Vector3 playerPos)
	{
		Vector3 direction = playerPos - enemyPos;
		direction.y = 0;
		direction.Normalize ();
		float distance = speed * Time.deltaTime;
		return enemyPos + new Vector3(direction.x,0, direction.z) * distance;
	}

	public void Attack()
	{
		if (cooldown <= 0 && player != null) {
			//Debug.Log("attacked player for "+ damage +" damage");
			player.ReceiveDamage(damage);
			cooldown = maxCoolDown;
		}
	}

	public void UpdateCooldown()
	{
		cooldown -= Time.deltaTime;
	}
}
