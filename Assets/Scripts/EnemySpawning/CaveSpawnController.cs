﻿using UnityEngine;
using System.Collections.Generic;

public class CaveSpawnController : MonoBehaviour {

	public float TowerRadius;
	public float SpawnRadius;
	public int NbSpawnPoints;

	private SpawnManager manager;
	public GameObject EnemyPrefab_Standard;	
	
	void Start(){
		if (TowerRadius == 0)
						TowerRadius = 5;
		if (SpawnRadius == 0)
						SpawnRadius = 30;
		if (NbSpawnPoints == 0)
						NbSpawnPoints = 8;


		manager = new SpawnManager ();

		manager.Path = new CavePath(TowerRadius, SpawnRadius, NbSpawnPoints);
		manager.WaveDataList = new WaveList01 ();
		
		manager.EnemyPrefab_Standard = this.EnemyPrefab_Standard;
	}
	
	void Update () {
		manager.Update();
	}

	public bool HasNextWave()
	{
		return manager.HasNextWave ();
	}

	public void StartNextWave()
	{
		manager.StartNextWave ();
	}

	public bool EnemiesUnspawnedInCurrentWave()
	{
		return manager.HasUnspawnedEnemiesRemaining ();
	}
}
