﻿using UnityEngine;
using System.Collections.Generic;

public class WaveList01 : WaveList {

	public WaveList01()
	{
		//config waves here
		var wave01 = new WaveData ();
		for (int i=0; i<100; i++) {
			wave01.AddEnemyAtTime (SpawnManager.EnemyTypes.STANDARD, i*0.5f);
		}
		waves.Add(wave01);
	}


}
