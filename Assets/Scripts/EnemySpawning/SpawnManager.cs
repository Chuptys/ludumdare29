﻿using UnityEngine;
using System.Collections;

public class SpawnManager {

	public WalkPath Path;
	public WaveList WaveDataList;
	
	private WaveData currentWave;
	private float timeElapsed = 0;
	private int enemyCounter = 0;
	
	public GameObject EnemyPrefab_Standard;	
	public enum EnemyTypes
	{
		STANDARD
	}

	public bool HasNextWave()
	{
		return WaveDataList.HasNextWave ();
	}
	
	public void StartNextWave()
	{
		if (!WaveDataList.HasNextWave())
			return;
		
		currentWave = WaveDataList.GetNextWaveData ();
		timeElapsed = 0;
		enemyCounter = 0;
	}
	
	public void Update () {
		if (Input.GetKeyDown (KeyCode.F)) {
			StartNextWave();
		}
		
		if (currentWave == null)
			return;
		
		timeElapsed += Time.deltaTime;
		
		if (enemyCounter > currentWave.EnemyList.Count - 1) {
			return;}		
		
		var e = currentWave.EnemyList[enemyCounter];
		var t = currentWave.SpawnTimes[enemyCounter];
		if (t <= timeElapsed) {
			SpawnEnemyOfType(e);
			enemyCounter++;
		}
	}

	public bool HasUnspawnedEnemiesRemaining()
	{
		if (currentWave == null)
						return false;

		return enemyCounter < currentWave.EnemyList.Count;
	}
	
	public void SpawnEnemyOfType(EnemyTypes type)
	{
		var newPath = Path.GetNew ();
		var spawnPos = newPath.GetStartPos ();
		switch (type) {
		case EnemyTypes.STANDARD:
			GameObject enemy = (GameObject.Instantiate(EnemyPrefab_Standard, spawnPos, Quaternion.identity) as GameObject);
			var script = enemy.GetComponent<EnemyScript>();
			script.Setup(newPath, 3f, 1f);
			break;
			//todo setup other enemy types here
		}
	}
}
