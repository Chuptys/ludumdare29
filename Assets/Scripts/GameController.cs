﻿using UnityEngine;
using System.Collections;

enum gameState {WaveInProgress, BuildPhase};

public class GameController : MonoBehaviour {

	private gameState gameState;
	public GameObject spawnController;
	public GameObject towerBuilder;
	public Camera firstPersonView;
	public Camera BuildPhaseCamera;
	public GameObject FirstPersonController;
	private Transform secondCamera;
	private Quaternion LastFirstPersonView;

	private Rect crosshairposition;
	public Texture2D crosshairTexture;


	private CaveSpawnController caveSpawner;

	void Start () {
		gameState = gameState.BuildPhase;
		//changeCameraToBuildPhase ();
		secondCamera = BuildPhaseCamera.transform;
		CharacterMotor t = FirstPersonController.GetComponent<CharacterMotor> () as CharacterMotor;
		t.canControl = false;
		firstPersonView.enabled = false;
		BuildPhaseCamera.enabled = true;
		LastFirstPersonView = firstPersonView.transform.rotation;
		crosshairposition = new Rect((Screen.width - crosshairTexture.width) / 2, (Screen.height - 
		                                                                     crosshairTexture.height) /2, crosshairTexture.width, crosshairTexture.height);

		caveSpawner = spawnController.GetComponent<CaveSpawnController> () as CaveSpawnController;
	}

	IEnumerator switchCamera(Transform from, Transform to, Camera camera, bool switchCamerasToFPV) {
		var animSpeed = 0.5f;
		
		Vector3 pos = from.position;
		Quaternion rot = from.rotation;
		
		float progress = 0.0f;  //This value is used for LERP
		while (progress < 1.0f)
		{
			camera.transform.position = Vector3.Lerp(pos, to.position, progress);
			camera.transform.rotation = Quaternion.Lerp(rot, to.rotation, progress);
			yield return new WaitForEndOfFrame();
			progress += Time.deltaTime * animSpeed;
		}
		
		//Set final transform
		camera.transform.position = to.position;
		camera.transform.rotation = to.rotation;

		if (switchCamerasToFPV) {
			CharacterMotor t = FirstPersonController.GetComponent<CharacterMotor> () as CharacterMotor;
			t.canControl = true;
			BuildPhaseCamera.enabled = false;
			firstPersonView.enabled = true;
			firstPersonView.transform.rotation = to.rotation;
			firstPersonView.transform.position = to.position;
		}
	}
	
	private void changeCameraToBuildPhase(){
		BuildPhaseCamera.transform.position = firstPersonView.transform.position;
		BuildPhaseCamera.transform.rotation = firstPersonView.transform.rotation;
		CharacterMotor t = FirstPersonController.GetComponent<CharacterMotor> () as CharacterMotor;
		LastFirstPersonView = firstPersonView.transform.rotation;
		t.canControl = false;
		firstPersonView.enabled = false;
		BuildPhaseCamera.enabled = true;
		StartCoroutine(switchCamera(firstPersonView.transform, secondCamera, BuildPhaseCamera,false));
	}

	private void changeCameraToWavePhase(){
		GameObject t2 = new GameObject();
		Transform t = t2.transform;
		t.position = firstPersonView.transform.position;
		t.rotation = LastFirstPersonView;
		StartCoroutine(switchCamera(secondCamera, t, BuildPhaseCamera,true));
	}

	void GameOver(){

	}


	void OnGUI() {
		if (gameState == gameState.WaveInProgress) {
			EnemyScript[] enemies = FindObjectsOfType(typeof(EnemyScript)) as EnemyScript[];
			foreach(EnemyScript enemy in enemies){
				enemy.drawGui(firstPersonView);
			}

			GUI.DrawTexture(crosshairposition, crosshairTexture);
		}
	}

	void Update () {
		if (gameState == gameState.WaveInProgress) {
			if(caveSpawner.EnemiesUnspawnedInCurrentWave()){
				return;
			} else {
				EnemyScript[] enemies = FindObjectsOfType(typeof(EnemyScript)) as EnemyScript[];
				if(enemies.Length == 0){
					if(caveSpawner.HasNextWave()){
						gameState = gameState.BuildPhase;
						changeCameraToBuildPhase();
					} else {
						GameOver();
					}
				}
			}
			//ask spawnController if wave is in progress -> change to gameState.BuildPhase, change camera to buildphase
		}
		if(gameState == gameState.BuildPhase){
			TowerBuilder tw = towerBuilder.GetComponent<TowerBuilder>() as TowerBuilder;
			if(!tw.toggle){
				tw.toggle = true;
			}
			if(tw.readyForNextWave){
				tw.toggle = false;
				tw.readyForNextWave = false;
				gameState = gameState.WaveInProgress;
				changeCameraToWavePhase();
				caveSpawner.StartNextWave();
			}
		}
	}
}
