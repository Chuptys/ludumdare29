﻿using UnityEngine;
using System.Collections.Generic;

public abstract class WalkPath {

	private int index = -1;
	protected List<Vector3> Nodes = new List<Vector3>();	
	protected bool initialized = false;

	public Vector3 GetStartPos()
	{
		initialize ();
		//Debug.Log ("WP: "+Nodes);

		return Nodes [0];
	}

	public Vector3 GetNextNode()
	{

		//Debug.Log ("Initialized: " + initialized);
		if (!initialized)
			initialize ();
		
		index++;

		Debug.Log ("NodeIndex: " + index + " " + Nodes[index]);
		return Nodes [index];
	}

	public bool HasNext()
	{
		return index < Nodes.Count-1;
	}

	private void initialize(){
		addNodes ();
		initialized = true;
	}

	public abstract WalkPath GetNew();


	protected abstract void addNodes();
}
