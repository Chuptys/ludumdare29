﻿using UnityEngine;
using System.Collections;

public class PlayerBulletMover : BulletController {

	public GameObject Cam;
	public float Speed;

	void Start () {
		this.gameObject.transform.position = new Vector3(Cam.transform.position.x, Cam.transform.position.y /*- 1*/, Cam.transform.position.z) + Cam.transform.forward * 1.5f;
		this.rigidbody.velocity = Speed * Cam.transform.forward;

		Quaternion bulletRotation = new Quaternion ();
		bulletRotation.SetFromToRotation (new Vector3(0,0,1.0f), this.rigidbody.velocity);
		this.rigidbody.rotation = bulletRotation;
	}

	void Update () {
		// TODO: CHECK IF BULLET IS OUT OF BOUNDS -> DESTROY
	}
}
