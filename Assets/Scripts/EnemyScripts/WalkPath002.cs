using UnityEngine;
using System.Collections;

public class WalkPath002 : WalkPath {	
	
	protected override void addNodes()
	{
		Nodes.Add (new Vector3 (-50, 26, 70));
		Nodes.Add (new Vector3 (-25, 26, 70));
		Nodes.Add (new Vector3 (-50, 26, 30));
	}

	public override WalkPath GetNew()
	{
		return new WalkPath002();
	}
}
