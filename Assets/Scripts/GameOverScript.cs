﻿using UnityEngine;
using System.Collections;

public class GameOverScript : MonoBehaviour {

	public GUIText gameOverText;
	// Use this for initialization
	void Start () {
		gameOverText.text = "Game Over!\n Your score is: " + PlayerPrefs.GetInt("CurrentScore");
	}
}
