﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
public class TowerBuilder : MonoBehaviour {
	
	public ParticleSystem smokeParticle;
	private List<GameObject> towerElements;

	public GameObject towerElement1;
	public GameObject towerElement2;
	public GameObject towerElement3;
	public GameObject towerElement4;
	public bool toggle;
	public bool readyForNextWave;

	private int money;
	private int score;

	void Start () {
		towerElements = new List<GameObject>();
		smokeParticle = Instantiate (smokeParticle, new Vector3 (0, 0, 0), Quaternion.identity) as ParticleSystem;
		toggle = false;
		money = 40;
		score = 0;
		readyForNextWave = false;
	}

	void DestroyElementOfTower() {
		if( towerElements.Count > 0){
			Destroy( towerElements[0]);
			towerElements.RemoveAt(0);
			smokeParticle.Play();
		}
	}

	public void DealDamage(float damage) {
		if (towerElements.Count > 0) {
			TowerElement other = (TowerElement)towerElements [0].GetComponent (typeof(TowerElement));
			other.dealDamage (damage);
			if (other.getHP () <= 0.0f) {
				DestroyElementOfTower ();		
			}
		}
	}

	public void CreateElementOfTower(GameObject element) {
		TowerElement towerElement = (TowerElement) element.GetComponent (typeof(TowerElement));
		if (money >= towerElement.price) {
			float currentHeight = (1 + (float)towerElements.Count) * element.gameObject.transform.localScale.y * 2.0f - 2.5f;
			GameObject newElement = Instantiate (element, new Vector3 (0, currentHeight, 0), Quaternion.identity) as GameObject;
			towerElements.Add (newElement);
			money -= towerElement.price;
		}
	}



	void Update () {
		/*if (Input.GetKeyDown (KeyCode.B)) {
			toggle = ! toggle;
		}*/
	}

	void OnGUI() {
		GUI.Box (new Rect (20, 20, 100, 40), "Score: " + score + "\nWealth: " + money);


		if (toggle) {
			int top = 80;
			GUI.Box (new Rect (20, top, 400, 500), "Tower Build Menu");
			int vcursor = top + 20;
			//Debug.Log (towerElements[0]);
			if (GUI.Button (new Rect (40, vcursor, 360, 20), "Build level 1")) {
				CreateElementOfTower (towerElement1);
			}
			vcursor += 20;
			if (GUI.Button (new Rect (40, vcursor, 360, 20), "Build level 2")) {
				CreateElementOfTower (towerElement2);
			}
			vcursor += 20;
			if (GUI.Button (new Rect (40, vcursor, 360, 20), "Build level 3")) {
				CreateElementOfTower (towerElement3);
			}
			vcursor += 20;
			if (GUI.Button (new Rect (40, vcursor, 360, 20), "Build level 4")) {
				CreateElementOfTower (towerElement4);
			}

			vcursor += 40;
			if (GUI.Button (new Rect (40, vcursor, 360, 20), "Start next wave!")) {
				readyForNextWave = true;
			}

			vcursor +=40;
			towerElements.ForEach(delegate(GameObject segment) {
				if (GUI.Button (new Rect (40, vcursor, 360, 20), "HP: "+(((TowerElement)(segment.GetComponent(typeof(TowerElement)))).getHP()))) {
					DealDamage(1.0f);
				}
				vcursor+=20;
			});		
		}
	}
}
