﻿using UnityEngine;
using System.Collections;

public class TowerElementFire : TowerElement {

	public GameObject Bullet;
	public float fireRate;
	private float nextFire = 0.0f;

	void shootAt(EnemyScript toShoot){
		Vector3 position = transform.position;
		Vector3 otherPosition = toShoot.transform.position;

		Vector3 spawnDirection = otherPosition - position;
		spawnDirection.y = 0.0f;
		Vector3 bulletPosition = position + spawnDirection.normalized * 6.0f;

		Vector3 shootDirection = (otherPosition - bulletPosition).normalized;

		Quaternion bulletRotation = new Quaternion ();
		bulletRotation.SetFromToRotation (new Vector3(0,1.0f,0), shootDirection);

		GameObject bulletSpawned = Instantiate (Bullet, bulletPosition, bulletRotation) as GameObject;
		//bulletSpawned.
	}

	void shoot() {
		EnemyScript[] enemies = FindObjectsOfType(typeof(EnemyScript)) as EnemyScript[];
		if (enemies.Length == 0) {
			return;		
		}
		EnemyScript closest = enemies[0];
		float distance = float.MaxValue;
		foreach (EnemyScript enemy in enemies) {
			float distanceToThisEnemy = (transform.position - enemy.transform.position).magnitude;
			if(distanceToThisEnemy < distance){
				distance = distanceToThisEnemy;
				closest = enemy;
			}
		}
		shootAt (closest);
	}

	void Update() {
		if (Time.time > nextFire) {
			nextFire = Time.time + fireRate;
			shoot();
		}
	}
}
