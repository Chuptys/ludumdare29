﻿using UnityEngine;
using System.Collections;

public class TwoPointPath : WalkPath {

	private Vector3 startNode;
	private Vector3 endNode;

	public TwoPointPath(Vector3 start, Vector3 end)
	{
		startNode = start;
		endNode = end;
	}

	protected override void addNodes()
	{
		Nodes.Add (startNode);
		Nodes.Add (endNode);
	}
	
	public override WalkPath GetNew()
	{
		return new TwoPointPath (startNode, endNode);
	}
}
