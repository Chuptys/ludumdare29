﻿using UnityEngine;
using System.Collections;

public class AttackTowerBehaviour {

	private float coolDown = 0f;
	private float maxCoolDown = 2f;
	private float damage;

	public AttackTowerBehaviour(float attackRate, float damageAmount)
	{
		maxCoolDown = attackRate;
		damage = damageAmount;
	}

	public void Update()
	{
		coolDown -= Time.deltaTime;
		if (coolDown <= 0) {
			coolDown = maxCoolDown;
			attackTower();
		}		 
	}

	private void attackTower()
	{
		Debug.Log ("Attacked Tower for "+damage+" damage");
	}
}
